import glob
import matplotlib.pyplot as plt
import pandas as pd 
import numpy as np
import re
import fnmatch
import seaborn as sns
from collections import MutableMapping 

###################
def plot_sel(df, x_var, y_var, xscale = 'linear', yscale = 'linear', bins = (50,50), range_var = [[0,250000],[0,250000]]):

    plt.hist2d(x = df[x_var], y = df[y_var], bins = bins, range = range_var)
    plt.colorbar()

    ax = plt.gca()
    ax.set_xscale(xscale)
    ax.set_yscale(yscale)
    
    plt.show()
    plt.close()
###################
def retro_dictify(frame):

    d = {}
    for row in frame.values:
        here = d
        for elem in row[:-2]:
            if elem not in here:
                here[elem] = {}
            here = here[elem]
        here[row[-2]] = row[-1]
    return d
###################
def convert_flatten(d, parent_key ='', sep ='_'): 

    items = [] 
    for k, v in d.items(): 
        new_key = parent_key + sep + str(k) if parent_key else str(k) 
  
        if isinstance(v, MutableMapping): 
            items.extend(convert_flatten(v, new_key, sep = sep).items()) 
        else: 
            items.append((new_key, v)) 
    return dict(items)
###################
def sel_events_based_on_remaining_frac(df_before_sel, df_after_sel, frac):
    
    df_before_sel_clone = df_before_sel.copy()
    df_after_sel_clone = df_after_sel.copy()
    
    counts_before_sel = df_before_sel_clone.groupby(['Time Point', 'Treatment', 'Biological Replicate', 'Technical Replicate']).size().reset_index(name='Counts')
    counts_after_sel  = df_after_sel_clone.groupby(['Time Point', 'Treatment', 'Biological Replicate', 'Technical Replicate']).size().reset_index(name='Counts')
    
    counts_after_sel['PassFSCWminFrac'] = np.where(counts_after_sel['Counts'] / counts_before_sel['Counts'] > 0.5, True, False)
    counts_after_sel.drop(['Counts'], axis=1, inplace = True)
    
    dic = retro_dictify(counts_after_sel)
    
    flatten_dic = convert_flatten(dic)
    
    for key, value in flatten_dic.items():
        if value == False:
            print("Sample " + key + " has been discarted.")
        
    for index, row in df_after_sel_clone.iterrows():
        df_after_sel_clone.at[index, 'PassFSCWminFrac'] = flatten_dic[ str(row['Time Point']) + '_' + row['Treatment'] + '_' + str(row['Biological Replicate']) + '_' + str(row['Technical Replicate']) ]
        
    df_after_sel_clone = df_after_sel_clone[df_after_sel_clone['PassFSCWminFrac'] == True]
    
    return df_after_sel_clone
###################
def add_missing_values(df_a, union):

    df = df_a.copy()

    for key in union:
        k = key.split('_')

        df_k = df[(df["Time Point"] == float(k[0])) & 
                                        (df["Treatment"] == k[1]) & 
                                        (df["Biological Replicate"] == float(k[2])) 
                                        & (df["Technical Replicate"] == float(k[3])) ]

        if(len(df_k) > 0):
            pass
        else:
            df = df.append({'Time Point' : float(k[0]) , 
                            'Treatment' : k[1],
                            'Biological Replicate' : float(k[2]),
                            'Technical Replicate' : float(k[3]),
                            'counts' : 0},  ignore_index=True )
            
    return df
###################
def fill_missing_samples(dfs):

    keys = []
    for df in dfs:
        keys.append(set(convert_flatten(retro_dictify(df)).keys()))
    
    union = keys[0]
    for k in keys[1:]:
        union = union | k

    filled_dfs = []
    for df in dfs:
        df_n = add_missing_values(df, union)
        df_n.sort_values(by =['Time Point', 'Treatment', 'Biological Replicate', 'Technical Replicate'], inplace = True)
        df_n = df_n.reset_index(drop=True)
        filled_dfs.append(df_n)

    return filled_dfs 
###################
    

     
    
